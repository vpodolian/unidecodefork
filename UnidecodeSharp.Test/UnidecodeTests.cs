﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace BinaryAnalysis.UnidecodeSharp.Test
{
    [TestFixture]
    class UnidecodeTests
    {
        [Test]
        public void DocTest()
        {
            Assert.AreEqual("Bei Jing ", "\u5317\u4EB0".Unidecode());
        }
        [Test]
        public void CustomTest()
        {
            Assert.AreEqual("Rabota s kirilitsiei", "Работа с кирилицей".Unidecode());
            Assert.AreEqual("aouoAOUO", "äöűőÄÖŨŐ".Unidecode());
        }
        [Test]
        public void PythonTest()
        {
            Assert.AreEqual("Hello, World!", "Hello, World!".Unidecode());

            Assert.AreEqual("'\"\r\n", "'\"\r\n".Unidecode());
            Assert.AreEqual("CZSczs", "ČŽŠčžš".Unidecode());
            Assert.AreEqual("a", "ア".Unidecode());
            Assert.AreEqual("a", "α".Unidecode());
            Assert.AreEqual("a", "а".Unidecode());
            Assert.AreEqual("chateau", "ch\u00e2teau".Unidecode());
            Assert.AreEqual("vinedos", "vi\u00f1edos".Unidecode());
        }

    }
}
